#pragma once
#include "kinect_grabber.h"
#include "linemod.h"
#include "session_manager.h"
#ifdef HAS_CURL
#include "sse_handler.h"
#endif
#include "hand_detector.h"
#include "opt_parse.h"
#include "mongoose.h"
#include "opt.h"
#include "ide_handler.h"
#include "audio_detector.h"
#include "opt.h"
#include "face_detector.h"
#include "image_sender.h"
#include "qr_creator.h"
#include "calibrator.h"
#include "upload.h"
#include "aliver.h"
#include "marker_viewer.h"
#include "audio_recorder.h"
#include "mutex.h"



